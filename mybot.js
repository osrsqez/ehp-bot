var fs = require('fs');
var request = require('request');
const discord = require("discord.js");
const client = new discord.Client();

global.currentLeader = 0;
global.ehps = [4];
global.names = ["Team Chapter", "Team Skater", "Team Vape", "Team Fyri"];
//global.tChapter = 0;
//global.tSkater  = 0;
//global.tVape    = 0;
//global.tFyri    = 0;

const tChapter = 0;
const tSkater  = 1;
const tVape    = 2;
const tFyri    = 3;

var compTotalUrl = "https://crystalmathlabs.com/tracker/api.php?type=comptotal&competition=";
var teamChapter = "16567";
var teamSkater = "16602";
var teamVape = "16566";
var teamFyri = "16605";

console.log("Loading the secret key:");
var secretKey = fs.readFileSync('secretkey','utf8');
client.on("ready", () => {
    console.log("ready");
});

let skills = JSON.parse(fs.readFileSync('skills.json'));

console.log(skills.ehp["mining"][0][1]);
//console.log(printSkillEhp("mining","all"));


//console.log(skills.level[10][1]);
//console.log(getLevelFromXp(1159));

client.on("message", (message) => {
    const prefix = "!ehp ";
    if(!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(("!ehp ").length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if(command == 'help'){
        message.channel.send("TODO: ADD HELP");
    }

    if(command == "rate"){
        let skill = args[0];
        let level = args[1];
        handleRate(message,skill,level);
    }


});

function handleRate(message,skill, level){
    skill = skill.toLowerCase();

    if(skill == "attack"){
//        message.channel.send("Requested info about Attack:");
    }
    if(skill == "defence"){}
    sendSkillEhp(message,skill,level);
}

function sendSkillEhp(message,skill,level){
    // if(level == "all"){
//        console.log(skills.ehp[skill]);
        for(let xp in skills.ehp[skill]){
            message.channel.send(getLevelFromXp(skills.ehp[skill][xp][0]) + ":" + skills.ehp[skill][xp][1]);
        }
    // }
}

function getCompTotals(){
    request(compTotalUrl+teamChapter, function(err,res,body){global.ehps[tChapter] = body});
    request(compTotalUrl+teamSkater,  function(err,res,body){global.ehps[tSkater] = body});
    request(compTotalUrl+teamVape,    function(err,res,body){global.ehps[tVape] = body});
    request(compTotalUrl+teamFyri,    function(err,res,body){global.ehps[tFyri] = body});
}

function getLevelFromXp(xp){
    for(i = 0; i < 126; i++){
//        if(skills.level[i][1] < xp &&  skills.level[i-1][1] >= xp){
//            return i-1;
//        }
//        console.log(skills.level[i][1] == xp);
        if(xp >= skills.level[i][1] && xp < skills.level[i+1][1]){
            console.log("Correct Logic");
            return i;
        }
    }
    return 0;
}


function checkLeader(){

    getCompTotals();
//    request(compTotalUrl+teamChapter, function(err,res,body){global.ehps[tChapter] = body});
//    request(compTotalUrl+teamSkater,  function(err,res,body){global.ehps[tSkater] = body});
//    request(compTotalUrl+teamVape,    function(err,res,body){global.ehps[tVape] = body});
//    request(compToalUrl+teamFyri,    function(err,res,body){global.ehps[tFyri] = body});

    //console.log("tChapter" + global.ehps[tChapter]);
    //console.log("tSkater"  + global.ehps[tSkater]);
    //console.log("tVape"    + global.ehps[tVape]);
    //console.log("tFyri"    + global.ehps[tFyri]);
    //global.currentLeader = 0;
    for(i = 0; i < 4; i++){
        if(parseFloat(global.ehps[i]) > parseFloat(global.ehps[global.currentLeader])){
            console.log("new leader");
            global.currentLeader = i;
        }
        //console.log("for team: " + global.names[i] + global.ehps[i] + " > " + global.ehps[global.currentLeader])
    } 

    console.log('The current leader is: ' + global.names[global.currentLeader]);

}

//check whos in the lead every 2 mins
//setTimeout(checkLeader, 2*60*1000);
setInterval(checkLeader, 10000);
client.login(secretKey);